package com.example.yunjeong.contentsview.util;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by YunSin on 2017-01-25.
 */

public class PostUtil {

    private OkHttpClient client = new OkHttpClient();

    public String run(String url) throws IOException {

        Request request = new Request.Builder()
                .url(url)
                .build();

        Response response = client.newCall(request).execute();

        if(!response.isSuccessful()) throw new IOException("Unexpected code" + response);

        return  response.body().string();
    }

}
