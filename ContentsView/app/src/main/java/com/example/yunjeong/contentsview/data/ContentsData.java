package com.example.yunjeong.contentsview.data;

public class ContentsData {

    private String title;
    private String author;
    private String fth;
    private int seq_contents;
    private String url_name;
    private String toparea;
    private String cornernew;
    private String genre;
    private String favorite;
    private String noti;
    private String described;
    private String top_cover_img;
    private String share;
    private String last_update;
    private String cover_img01;
    private String cover_img02;
    private String cover_img03;
    private String cover_img04;
    private String display;
    private String reg_date;
    private String ad_mobile_full;
    private String ad_mobile_banner;
    private String ad_mobile_rwd;
    private String group_id;
    private String group_title;
    private String group_img;
    private String group_img2;
    private String group_sort;
    private String admin_text;

    public String getUrl_name() {
        return url_name;
    }

    public String getToparea() {
        return toparea;
    }

    public String getCornernew() {
        return cornernew;
    }

    public String getGenre() {
        return genre;
    }

    public String getFavorite() {
        return favorite;
    }

    public String getNoti() {
        return noti;
    }

    public String getDescribed() {
        return described;
    }

    public String getTop_cover_img() {
        return top_cover_img;
    }

    public String getShare() {
        return share;
    }

    public String getLast_update() {
        return last_update;
    }

    public String getCover_img01() {
        return cover_img01;
    }

    public String getCover_img02() {
        return cover_img02;
    }

    public String getCover_img03() {
        return cover_img03;
    }

    public String getCover_img04() {
        return cover_img04;
    }

    public String getDisplay() {
        return display;
    }

    public String getReg_date() {
        return reg_date;
    }

    public String getAd_mobile_full() {
        return ad_mobile_full;
    }

    public String getAd_mobile_banner() {
        return ad_mobile_banner;
    }

    public String getAd_mobile_rwd() {
        return ad_mobile_rwd;
    }

    public String getGroup_id() {
        return group_id;
    }

    public String getGroup_title() {
        return group_title;
    }

    public String getGroup_img() {
        return group_img;
    }

    public String getGroup_img2() {
        return group_img2;
    }

    public String getGroup_sort() {
        return group_sort;
    }

    public String getAdmin_text() {
        return admin_text;
    }

    public int getSeq_contents() {
        return seq_contents;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public String getFth() {
        return fth;
    }

}
