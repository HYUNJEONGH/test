package com.example.yunjeong.contentsview.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.yunjeong.contentsview.Config;
import com.example.yunjeong.contentsview.R;
import com.example.yunjeong.contentsview.data.Chapters;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by YunSin on 2017-02-03.
 */

public class ChaptersAdapter extends RecyclerView.Adapter<ChaptersAdapter.ViewHolder> {
    private final Context context;
    private final List<Chapters> chapterList;

    public ChaptersAdapter(Context context, List<Chapters> chapterList) {
        this.context = context;
        this.chapterList = chapterList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.chapter_list_item, parent, false);
        return new ViewHolder(v);
    }

    //Replace the contents of a view.
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Glide.with(context).load(Config.CONTENT_URL_PREFIX+chapterList.get(position).getCh_img()).into(holder.mImgView);
        holder.mTitle.setText(chapterList.get(position).getTitle());
        String open_type = chapterList.get(position).getOpen_type();

        if(open_type.equals(Config.OPEN_TYPE_FREE))
            holder.mOpenType.setText("FREE");
        else if (open_type.equals(Config.OPEN_TYPE_AD))
            holder.mOpenType.setText("AD");
        else
            holder.mOpenType.setText("TOKEN");
    }

    @Override
    public int getItemCount() {
        return chapterList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final ImageView mImgView;
        private final TextView mTitle;
        private final TextView mOpenType;

        public ViewHolder(View v) {
            super(v);
            mImgView = (ImageView)v.findViewById(R.id.img_ch);
            mTitle = (TextView)v.findViewById(R.id.ch_title);
            mOpenType = (TextView)v.findViewById(R.id.ch_openType);
        }
    }
}
