package com.example.yunjeong.contentsview.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import com.example.yunjeong.contentsview.Config;
import com.example.yunjeong.contentsview.R;
import com.example.yunjeong.contentsview.data.ContentsData;
import com.example.yunjeong.contentsview.listener.OnContentsItemSelectedListener;

public class ContentsAdapter extends BaseAdapter{

    private Context mContext;
    private List<ContentsData> mList;
    LayoutInflater inflater;
    private OnContentsItemSelectedListener onContentsItemSelectedListener;

    public ContentsAdapter(Context context, List<ContentsData> list) {
        mContext = context;
        mList = list;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.onContentsItemSelectedListener = (OnContentsItemSelectedListener)context;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder;
        if(view == null) {
            view = inflater.inflate(R.layout.contents_item, parent, false);
            holder = new ViewHolder();
            holder.tvTitle = (TextView)view.findViewById(R.id.tvTitle);
            holder.tvAuthor = (TextView)view.findViewById(R.id.tvAuthor);
            holder.imgTop = (ImageView)view.findViewById(R.id.imageView);
            holder.contentsItem = (LinearLayout)view.findViewById(R.id.contentsItem);
            view.setTag(holder);
        } else {
            holder = (ViewHolder)view.getTag();
        }

        holder.tvTitle.setText(mList.get(position).getTitle());
        holder.tvAuthor.setText(mList.get(position).getAuthor());
        Glide.with(mContext).load(Config.CONTENT_URL_PREFIX+mList.get(position).getFth()).into(holder.imgTop);
        holder.contentsItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onContentsItemSelectedListener.onContentsItemClick(v, mList.get(position).getSeq_contents());
            }
        });
        return view;
    }

    static class ViewHolder {
        TextView tvTitle;
        TextView tvAuthor;
        ImageView imgTop;
        LinearLayout contentsItem;
    }

}
