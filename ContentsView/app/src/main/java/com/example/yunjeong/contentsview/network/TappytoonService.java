package com.example.yunjeong.contentsview.network;

import com.example.yunjeong.contentsview.data.Chapters;
import com.example.yunjeong.contentsview.data.ContentsData;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by YunSin on 2017-02-07.
 */

public interface TappytoonService {
    @GET("contents/chaptersList")
    Call<List<Chapters>> getChapterList(@Query("content_id") int content_id);

    @GET("contents/contentsList")
    Call<List<ContentsData>> getContent();

}
