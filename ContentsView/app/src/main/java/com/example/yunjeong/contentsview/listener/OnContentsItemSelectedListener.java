package com.example.yunjeong.contentsview.listener;

import android.view.View;

/**
 * Created by YunSin on 2017-02-03.
 */

public interface OnContentsItemSelectedListener {
    void onContentsItemClick(View view, int seq_chapters);
}

