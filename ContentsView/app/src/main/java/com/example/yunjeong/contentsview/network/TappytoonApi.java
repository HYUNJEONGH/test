package com.example.yunjeong.contentsview.network;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by yunjeonghwang on 2017. 2. 5..
 */

public class TappytoonApi {

    private static TappytoonApi instance;
    private TappytoonService service;

    private TappytoonApi() {}

    public static TappytoonApi get() {
        if (instance == null) {
            instance = new TappytoonApi();
        }
        return instance;
    }

    public TappytoonService getRetrofitService() {
        if(service == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("http://52.36.207.202:8080/api/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            service = retrofit.create(TappytoonService.class);
        }
        return  service;
    }

}

