package com.example.yunjeong.contentsview.ui;

import android.content.Intent;
import android.provider.Settings.Secure;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.GridView;

import com.example.yunjeong.contentsview.Config;
import com.example.yunjeong.contentsview.R;
import com.example.yunjeong.contentsview.listener.OnContentsItemSelectedListener;
import com.example.yunjeong.contentsview.network.TappytoonApi;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.example.yunjeong.contentsview.data.ContentsData;
import com.example.yunjeong.contentsview.util.PostUtil;
import com.example.yunjeong.contentsview.adapter.ContentsAdapter;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements OnContentsItemSelectedListener {

    private List<ContentsData> contentsList;
    private Call<List<ContentsData>> contentsListt;
    GridView gridView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final String android_id= Secure.getString(this.getContentResolver(), Secure.ANDROID_ID);

        gridView = (GridView)findViewById(R.id.gridview);

        MobileAds.initialize(getApplicationContext(), "ca-app-pub-6176541133595274~4527883944");
        AdView mAdView = (AdView)findViewById(R.id.ad_banner_view);

        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(android_id)
                .build();

        mAdView.loadAd(adRequest);

//        new getContents().execute();
        getContents();
    }

    //ContentsList
    public void getContents() {
        contentsListt = TappytoonApi.get().getRetrofitService().getContent();

        contentsListt.enqueue(new Callback<List<ContentsData>>() {
            @Override
            public void onResponse(Call<List<ContentsData>> call, Response<List<ContentsData>> response) {
                if(response != null && response.body() != null) {
                    List<ContentsData> list = response.body();
                    gridView.setAdapter(new ContentsAdapter(MainActivity.this, list));
                }
            }

            @Override
            public void onFailure(Call<List<ContentsData>> call, Throwable t) {

            }
        });
    }

    @Override
    public void onContentsItemClick(View view, int seq_chapters) {
        goToChapterList(seq_chapters);
    }

    private void goToChapterList(int seq_chapters) {
        Intent intent = new Intent(this, ChapterListActivity.class);
        intent.putExtra("seq_chapters", seq_chapters);
        startActivity(intent);
    }
/*
    public class getContents extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {

            String response;
            PostUtil postUtil = new PostUtil();
            try {
                response = postUtil.run(Config.URL_GETCONTENTS);
//                Log.d("obj", response);
                JSONArray contents = new JSONArray(response);
                contentsList = new ArrayList<ContentsData>();
                for(int i=0; i < contents.length(); i++) {
                    JSONObject item = contents.getJSONObject(i);
                    contentsList.add(new ContentsData(item.getString("title"), item.getString("author"), item.getString("fth"), item.getInt("seq_contents")));
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        gridView.setAdapter(new ContentsAdapter(MainActivity.this, contentsList));
                    }
                }
            );
        }

    }*/
}