package com.example.yunjeong.contentsview.ui;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.example.yunjeong.contentsview.Config;
import com.example.yunjeong.contentsview.R;
import com.example.yunjeong.contentsview.adapter.ChaptersAdapter;
import com.example.yunjeong.contentsview.data.Chapters;
import com.example.yunjeong.contentsview.network.TappytoonApi;
import com.example.yunjeong.contentsview.util.PostUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChapterListActivity extends AppCompatActivity {
    private List<Chapters> chapterList;
    private RecyclerView chapterView;
    private Call<List<Chapters>> list;
    private int seq_chapters;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chapter_list);
        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            seq_chapters = extras.getInt("seq_chapters");
        }
        chapterView = (RecyclerView)findViewById(R.id.chapterList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        chapterView.setLayoutManager(layoutManager);

//        new getChapters().execute(seq_chapters);

        getChapterList(seq_chapters);
    }

    public void getChapterList(int seq_chapters) {
        list = TappytoonApi.get().getRetrofitService().getChapterList(seq_chapters);
        list.enqueue(new Callback<List<Chapters>>() {
            @Override
            public void onResponse(Call<List<Chapters>> call, Response<List<Chapters>> response) {
                if(response != null && response.body() != null) {
                    List<Chapters> list = response.body();
                    chapterView.setAdapter(new ChaptersAdapter(ChapterListActivity.this, list));
                }
            }
            @Override
            public void onFailure(Call<List<Chapters>> call, Throwable t) {

            }
        });
    }
/*
    public class getChapters extends AsyncTask<Integer, Void, Void> {
        @Override
        protected Void doInBackground(Integer... params) {
            String response;
            String param = Integer.toString(params[0]);
            PostUtil postUtil = new PostUtil();
            try {
                response = postUtil.run(Config.URL_GETCHAPTERS+param);
                JSONArray contents = new JSONArray(response);
                chapterList = new ArrayList<Chapters>();
                for(int i=0; i < contents.length(); i++) {
                    JSONObject item = contents.getJSONObject(i);
                    chapterList.add(new Chapters(item.getString("title"), item.getString("ch_img"), item.getString("open_type")));
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            chapterView.setAdapter(new ChaptersAdapter(getApplicationContext(), chapterList));
        }
    }*/
}
