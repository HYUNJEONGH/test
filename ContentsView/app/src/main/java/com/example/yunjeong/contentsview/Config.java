package com.example.yunjeong.contentsview;

/**
 * Created by yunjeonghwang on 2017. 2. 6..
 */

public class Config {
    //content_url
    public static final String CONTENT_URL_PREFIX = "http://d1ed0vta5mrb00.cloudfront.net/";

    //chapterList url
//    public static String URL_GETCHAPTERS = "http://52.36.207.202:8080/api/contents/chaptersList?";

    //contentList url
//    public static String URL_GETCONTENTS = "http://52.36.207.202:8080/api/contents/contentsList";


    //chapter open_type
    public static final String OPEN_TYPE_FREE = "00";
    public static final String OPEN_TYPE_AD = "01";
    public static final String OPEN_TYPE_TOKEN = "02";
    public static final String OPEN_TYPE_DATE = "03";
}
