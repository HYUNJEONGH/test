package com.example.yunjeong.contentsview.data;

/**
 * Created by YunSin on 2017-02-03.
 */

public class Chapters {
    private int seq_chapters;
    private String title;
    private String subtitle;
    private String ch_img;
    private int chapter_price;
    private String url;
    private String up;
    private String display;
    private String buy_chp;
    private String open_type;
    private String open_date;
    private String show_type;
    private String app_ver;
    private String reg_date;

    public int getSeq_chapters() {
        return seq_chapters;
    }

    public String getTitle() {
        return title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public String getCh_img() {
        return ch_img;
    }

    public int getChapter_price() {
        return chapter_price;
    }

    public String getUrl() {
        return url;
    }

    public String getUp() {
        return up;
    }

    public String getDisplay() {
        return display;
    }

    public String getBuy_chp() {
        return buy_chp;
    }

    public String getOpen_type() {
        return open_type;
    }

    public String getOpen_date() {
        return open_date;
    }

    public String getShow_type() {
        return show_type;
    }

    public String getApp_ver() {
        return app_ver;
    }

    public String getReg_date() {
        return reg_date;
    }
}


